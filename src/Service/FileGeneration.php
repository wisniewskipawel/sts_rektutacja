<?php

namespace App\Service;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Writer\Csv;


class FileGeneration {
    
    public function file($value)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle('Liasta operacji');
        $sheet->getCell('A1')->setValue('wallet balance');
        $sheet->getCell('B1')->setValue('Operacton amount');
        $sheet->getCell('C1')->setValue('Operation description');
        $sheet->getCell('D1')->setValue('Date operation');
        $sheet->fromArray($value,null, 'A2', true);
        $writer = new Xlsx($spreadsheet);
        $writer->save('lista operacji.xlsx');

    }
    
    public function fileCsv($value)
    {

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->fromArray($value);

        $writer = new Csv($spreadsheet);
        $writer->save('lista operacji.csv');

    }
}

?>