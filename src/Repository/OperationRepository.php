<?php

namespace App\Repository;

use App\Entity\Operation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Join;
use App\Entity\Wallet;

/**
 * @method Operation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Operation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Operation[]    findAll()
 * @method Operation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OperationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Operation::class);
    }
    /*
    public function allOperations($wallet_id)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.wallet = :val')
            ->setParameter('val', $wallet_id)
            ->orderBy('o.id', 'ASC')
            ->getQuery()
            ->getResult()
        ;
        
    }
    */
    public function joinOperationsClient($wallet_id)
    {

        return $this->createQueryBuilder('ope')
            ->select('wallet.id as wallet_id','
                     wallet.balance as wallet_balance',
                     'ope.amount as operacton_amount',
                     'ope.description as operation_description',
                     'ope.createAd as date_operation')
            ->innerJoin(Wallet::class, 'wallet',  Join::WITH, 'ope.wallet = wallet.id' )
            ->andWhere('wallet.id = :wallet_id')
            ->setParameter('wallet_id', $wallet_id)
            ->orderBy('ope.id', 'ASC')
            ->getQuery()
            ->getResult()
            ;
    }

    // /**
    //  * @return Operation[] Returns an array of Operation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Operation
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
