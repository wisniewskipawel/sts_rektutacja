<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\OperationRepository;
use App\Service\FileGeneration;


/**
* @Route("/operations")
*/
class OperationsController extends AbstractController
{
    private $operationRepository;
    
    public function __construct(OperationRepository $operationRepository)
    {
        $this->operationRepository = $operationRepository;
    }
        
    /**
    * @Route("/{wallet_id}/show", name="operations_show")
    */
    public function allOperations(int $wallet_id): Response
    {
       $alloperation = $this->operationRepository->joinOperationsClient($wallet_id);
      if(!$alloperation)
      {
        $this->addFlash("error", "We haven't registered any operations yet");
        return $this->redirectToRoute('clients');
      }

        return $this->render('operations/index.html.twig', [
            'alloperations' => $alloperation,
        ]);
    }
    
    /**
    * @Route("/{wallet_id}/file", name="operations_file")
    */
    public function fileGenerate(int $wallet_id,FileGeneration $fileGeneration): Response
    {
        $alloperation = $this->operationRepository->joinOperationsClient($wallet_id);
         
        if(!$alloperation)
        {
          $this->addFlash("error", "We haven't registered any operations yet");
          return $this->redirectToRoute('clients');
        }
        $fileGeneration->file($alloperation);
        $this->addFlash("success", "File is generated. Download file from public catalog");        
        return $this->redirectToRoute('clients');
    }
    
    
    /**
    * @Route("/{wallet_id}/file/csv", name="operations_file_csv")
    */
    public function fileGenerateCsv(int $wallet_id,FileGeneration $fileGeneration): Response
    {
        $alloperation = $this->operationRepository->joinOperationsClient($wallet_id);
         
        if(!$alloperation)
        {
          $this->addFlash("error", "We haven't registered any operations yet");
          return $this->redirectToRoute('clients');
        }
        $fileGeneration->fileCsv($alloperation);
        $this->addFlash("success", "File is generated. Download file from public catalog");        
        return $this->redirectToRoute('clients');
    }
}