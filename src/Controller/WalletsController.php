<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\WalletRepository;

/**
* @Route("/wallets")
*/
class WalletsController extends AbstractController
{
    
    private $walletRepository;
    
    public function __construct(WalletRepository $walletRepository)
    {
        $this->walletRepository = $walletRepository;
    }
    
    /**
     * @Route("/{id}", name="wallets_show")
     */
    public function index(): Response
    {
        return $this->render('wallets/index.html.twig', [
            'controller_name' => 'WalletsController',
        ]);
    }
}
