<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ClientRepository;

/**
* @Route("/clients")
*/
class ClientsController extends AbstractController
{
        
    private $clientRepository;
    
    public function __construct(ClientRepository $clientRepository)
    {    
        $this->clientRepository = $clientRepository;
    }
        
    /**
     * @Route("/", name="clients")
     */
    public function index(): Response
    {
        $clients = $this->clientRepository->allClient();
        
        if(!$clients)
        {
            $this->addFlash('error', 'Sorry, there are no customers yet');
            return $this->redirectToRoute('start');
        }
        
        return $this->render('clients/index.html.twig', [
            'clients' => $clients,
        ]);
    }
}
